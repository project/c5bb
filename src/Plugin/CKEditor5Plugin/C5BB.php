<?php

namespace Drupal\c5bb\Plugin\CKEditor5Plugin;

use Drupal\ckeditor5\Plugin\CKEditor5PluginConfigurableInterface;
use Drupal\ckeditor5\Plugin\CKEditor5PluginConfigurableTrait;
use Drupal\ckeditor5\Plugin\CKEditor5PluginDefault;
use Drupal\ckeditor5\Plugin\CKEditor5PluginElementsSubsetInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\EditorInterface;

/**
 * CKEditor 5 C5BB plugin configuration.
 *
 * @internal
 *   Plugin classes are internal.
 */
class C5BB extends CKEditor5PluginDefault implements CKEditor5PluginConfigurableInterface, CKEditor5PluginElementsSubsetInterface {

  use CKEditor5PluginConfigurableTrait;

  const T_CONTEXT = ['context' => 'C5BB module'];

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['classes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Class Selectors', [], self::T_CONTEXT),
      '#description' => $this->t('Configuration of class options.', [], self::T_CONTEXT),
      '#default_value' => $this->configuration['classes'] ?? '',
      '#rows' => 10,
    ];

    $form['textClass'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text Class', [], self::T_CONTEXT),
      '#description' => $this->t('Class of text container (span tag) of a button.', [], self::T_CONTEXT),
      '#default_value' => $this->configuration['textClass'],
    ];

    $form['showIconSettings'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show Icon Setttings', [], self::T_CONTEXT),
      '#description' => $this->t('Enbales Glyph and FontAwesome icons UI.', [], self::T_CONTEXT),
      '#default_value' => $this->configuration['showIconSettings'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    ;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['classes'] = $form_state->getValue('classes');
    $this->configuration['textClass'] = Html::getClass(trim($form_state->getValue('textClass')));
    $this->configuration['showIconSettings'] = $form_state->getValue('showIconSettings');
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'classes' => <<<__classes__
Size
- Small|btn-sm
- Normal|
- Large|btn-lg
Style
- Primary|btn-primary
- Secondary|btn-secondary
Color
- Light|
- Dark|dark
__classes__,
      'textClass' => 'text',
      'showIconSettings' => 1,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getElementsSubset(): array {
    return ['<a>', '<a class target href>', '<em>', '<em class>', '<span>', '<span class>'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDynamicPluginConfig(array $static_plugin_config, EditorInterface $editor): array {
    // Build selector's config.
    $classes = explode(PHP_EOL, $this->configuration['classes']);
    $selectors = [];
    foreach ($classes as $line) {
      $line = trim($line);
      if (empty($line)) {
        continue;
      }
      if ($line[0] != '-') {
        $selectors[] = [
          'label' => $this->t($line, [], self::T_CONTEXT),
          'options' => [],
        ];
      }
      else {
        $line = explode('|', $line);
        $optionLabel = trim(substr($line[0], 1));
        $optionValue = trim($line[1] ?? '');
        $index = count($selectors) - 1;
        $selectors[$index]['options'][] = [
          'value' => $optionValue,
          'label' => $this->t($optionLabel, [], self::T_CONTEXT),
        ];
      }
    }

    return [
      'c5bb' => $this->configuration + ['selectors' => $selectors],
    ];
  }

}
