/**
 * @file
 * Defines the BButtonEditing plugin.
 */

/**
 * @module bButton/BButtonEditing
 */

import {Plugin} from 'ckeditor5/src/core';
import BButtonCommand from "./bbuttoncommand";
import ClassCollector from './ui/classcollection';

/**
 * The editing feature.
 *
 * It introduces the 'bButton' element in the model.
 *
 * @extends module:core/plugin~Plugin
 */
export default class BButtonEditing extends Plugin {

  /**
   * @inheritDoc
   */
  init() {
    this._defineSchema();
    this._defineConverters();

    const editor = this.editor;

    // Attaching the command to the editor.
    editor.commands.add(
      'bButton',
      new BButtonCommand(this.editor),
    );
  }

  /**
   * Registers schema for bbutton and its child elements.
   *
   * @private
   */
  _defineSchema() {
    const schema = this.editor.model.schema;
    const textFormatSettings = this.editor.config.get('c5bb')
    const customAttributes = [];
    for (let index = 0; index < textFormatSettings.selectors.length; index++) {
      customAttributes.push('bbSelector_' + index);
    }

    // parent element.
    schema.register('bButton', {
      inheritAllFrom: '$inlineObject',
      allowAttributes: [
        'bbLinkUrl',
        ...customAttributes,
        'bbTarget',
        // Needed to avoid conflicts with the 'linkClass' model attribute.
        'bbLinkClass'
      ],
      allowChildren: [
        'bbGLeft',
        'bbGRight',
        'bbFALeft',
        'bbFARight',
        'bbLinkText',
      ],
    });

    // child elements.
    schema.register('bbLinkText', {
      allowIn: 'bButton',
      isLimit: true,
      allowContentOf: '$block',
    });
    schema.register('bbGLeft', {
      allowIn: 'bButton',
      isLimit: true,
      allowContentOf: '$block',
      allowAttributes: ['value']
    });
    schema.register('bbGRight', {
      allowIn: 'bButton',
      isLimit: true,
      allowContentOf: '$block',
      allowAttributes: ['value']
    });
    schema.register('bbFALeft', {
      allowIn: 'bButton',
      isLimit: true,
      allowContentOf: '$block',
      allowAttributes: ['value']
    });
    schema.register('bbFARight', {
      allowIn: 'bButton',
      isLimit: true,
      allowContentOf: '$block',
      allowAttributes: ['value']
    });
  }

  /**
   * Defines converters.
   */
  _defineConverters() {
    const {conversion} = this.editor;
    const textFormatSettings = this.editor.config.get('c5bb')

    // bButton. View -> Model.
    conversion.for('upcast').elementToElement({
      view: {
        name: 'a',
        classes: [ 'btn' ],
        attributes: {
          ['href']: true,
          ['class']: true
        }
      },
      converterPriority: 'highest',
      model: (viewElement, conversionApi ) => {
        // Do not convert if the link does not have the 'btn' class.
        let classes = viewElement.getAttribute('class');
        if (!classes) {
          return null;
        }
        classes = classes.split(' ');
        if (!classes.includes('btn')) {
          return null;
        }

        var attrs = {
          bbLinkUrl: viewElement.getAttribute('href'),
          bbLinkClass: classes.join(' '),
          bbTarget: viewElement.getAttribute('target') ?? '',
        };

        for (let index = 0; index < textFormatSettings.selectors.length; index++) {
          attrs['bbSelector_' + index] = '';
          textFormatSettings.selectors[index].options.forEach((elm) => {
            if (classes.includes(elm.value)) {
              attrs['bbSelector_' + index] = elm.value;
            }
          })
        }

        return conversionApi.writer.createElement( 'bButton', attrs );
      },
    });

    // two next upcasts has only purpose to override
    // core Link conversions
    conversion.for('upcast').attributeToAttribute({
      view: {
        name: 'a',
        attributes: {
          ['href']: true
        }
      },
      model: {
        key: 'bbLinkUrl',
        value: viewElement => {
          return viewElement.getAttribute('href');
        }
      },
    });

    // Needed to avoid conflicts with the 'linkClass' model attribute
    // bound to the same HTML "class" attribute.
    conversion.for('upcast').attributeToAttribute({
      view: {
        name: 'a',
        attributes: {
          ['class']: true
        }
      },
      model: {
        key: 'bbLinkClass',
        value: viewElement => {
          return viewElement.getAttribute('class');
        }
      },
    });

    // bButton. Model -> View.
    conversion.for('downcast').elementToElement({
      model: 'bButton',
      view: (modelElement, { writer }) => {
        const classes = ['btn'];

        for (let index = 0; index < textFormatSettings.selectors.length; index++) {
          if (modelElement.getAttribute('bbSelector_' + index)) {
            classes.push(modelElement.getAttribute('bbSelector_' + index))
          }
        }

        var htmlAttrs = {
          'class': classes.join(' '),
          'href': modelElement.getAttribute('bbLinkUrl'),
        };

        if (modelElement.getAttribute('bbTarget')) {
          htmlAttrs.target = modelElement.getAttribute('bbTarget');
        }

        return writer.createContainerElement('a', htmlAttrs);
      }
    });

    // bButtonText. View -> Model.
    conversion.for('upcast').elementToElement({
      view: {
        name: 'span',
        classes: textFormatSettings.textClass,
      },
      model: ( _, { writer } ) => {
        return writer.createElement('bbLinkText');
      }
    });

    // Model -> View.
    conversion.for('downcast').elementToElement({
      model: 'bbLinkText',
      view: ( _, { writer: viewWriter } ) => {
        return viewWriter.createContainerElement('span', textFormatSettings.textClass
          ? {class: textFormatSettings.textClass}
          : {}
        );
      }
    });

    // bbFALeft. View -> Model.
    conversion.for('upcast').elementToElement({
      view: {
        name: 'em',
        classes: ['fa', 'fontawesome-icon-left'],
      },
      converterPriority: 'highest',
      model: ( viewElement, { writer } ) => {
        if (!viewElement.getAttribute('class')) return null;
        const classes = (new ClassCollector()).addFromString(viewElement.getAttribute('class'));
        if (!classes.has('fa') || !classes.has('fontawesome-icon-left')) {
          return null;
        }

        var attrs = {
          value: classes.remove('fa').remove('fontawesome-icon-left').getClassName()
        };
        return writer.createElement( 'bbFALeft', attrs );
      }
    });

    // Model -> View.
    conversion.for('downcast').elementToElement({
      model: 'bbFALeft',
      view: ( modelElement, { writer: viewWriter } ) => {
        const classes = new ClassCollector(['fa', 'fontawesome-icon-left']);
        if (modelElement.getAttribute('value')) {
          classes.addFromString(modelElement.getAttribute('value'));
        }
        return viewWriter.createAttributeElement('em', { class: classes.getClassName() });
      }
    });

    // bbFARight. View -> Model.
    conversion.for('upcast').elementToElement({
      view: {
        name: 'em',
        classes: ['fa', 'fontawesome-icon-right'],
      },
      model: ( viewElement, { writer } ) => {
        if (!viewElement.getAttribute('class')) return null;
        const classes = (new ClassCollector()).addFromString(viewElement.getAttribute('class'));
        if (!classes.has('fa') || !classes.has('fontawesome-icon-right')) {
          return null;
        }

        var attrs = {
          value: classes.remove('fa').remove('fontawesome-icon-right').getClassName()
        };
        return writer.createElement( 'bbFARight', attrs);
      }
    });

    // Model -> View.
    conversion.for('downcast').elementToElement({
      model: 'bbFARight',
      view: ( modelElement, { writer: viewWriter } ) => {
        const classes = new ClassCollector(['fa', 'fontawesome-icon-right']);
        if (modelElement.getAttribute('value')) {
          classes.addFromString(modelElement.getAttribute('value'));
        }
        return viewWriter.createAttributeElement('em', { class: classes.getClassName() });
      }
    });

    // bbGLeft. View -> Model.
    conversion.for('upcast').elementToElement({
      view: {
        name: 'span',
        classes: ['bs-icon-left', 'glyphicon'],
      },
      model: ( viewElement, { writer } ) => {
        const classesValue = viewElement.getAttribute('class');
        if (!classesValue) return null;
        const classes = (new ClassCollector()).addFromString(classesValue);
        if (!classes.has('glyphicon') || !classes.has('bs-icon-left')) {
          return null;
        }
        const attrs = {
          value: classes.remove('glyphicon').remove('bs-icon-left').getClassName()
        };
        return writer.createElement( 'bbGLeft', attrs);
      }
    });

    // Model -> View.
    conversion.for('downcast').elementToElement({
      model: 'bbGLeft',
      view: ( modelElement, { writer: viewWriter } ) => {
        const classes = new ClassCollector(['glyphicon', 'bs-icon-left']);
        if (modelElement.getAttribute('value')) {
          classes.addFromString(modelElement.getAttribute('value'));
        }
        return viewWriter.createAttributeElement('span', { class: classes.getClassName() });
      }
    });

    // bbGRight. View -> Model.
    conversion.for('upcast').elementToElement({
      view: {
        name: 'span',
        classes: ['bs-icon-right', 'glyphicon'],
      },
      model: ( viewElement, { writer } ) => {
        const classesValue = viewElement.getAttribute('class');
        if (!classesValue) return null;
        const classes = (new ClassCollector()).addFromString(classesValue);
        if (!classes.has('glyphicon') || !classes.has('bs-icon-right')) {
          return null;
        }
        const attrs = {
          value: classes.remove('glyphicon').remove('bs-icon-right').getClassName()
        };
        return writer.createElement( 'bbGRight', attrs);
      }
    });

    // Model -> View.
    conversion.for('downcast').elementToElement({
      model: 'bbGRight',
      view: ( modelElement, { writer: viewWriter } ) => {
        const classes = new ClassCollector(['glyphicon', 'bs-icon-right']);
        if (modelElement.getAttribute('value')) {
          classes.addFromString(modelElement.getAttribute('value'));
        }
        return viewWriter.createAttributeElement('span', { class: classes.getClassName() });
      }
    });

  }
}
