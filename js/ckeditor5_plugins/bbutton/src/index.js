/**
 * @file
 * Defines the BButton plugin.
 */

/**
 * @module bbutton/BButton
 */

import { Plugin } from 'ckeditor5/src/core';
import BButtonEditing from './bbuttonediting';
import BButtonUI from './bbuttonui';

/**
 * The BButton plugin.
 *
 * This is a "glue" plugin that loads
 *
 * @extends module:core/plugin~Plugin
 */
class BButton extends Plugin {

  /**
   * @inheritdoc
   */
  static get requires() {
    return [BButtonEditing, BButtonUI];
  }

  /**
   * @inheritdoc
   */
  static get pluginName() {
    return 'bButton';
  }

}

export default {
  BButton,
};
