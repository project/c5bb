/**
 * @file
 * Defines the BButtonUI plugin.
 */

/**
 * @module bbutton/BButtonUI
 */

import { Plugin } from 'ckeditor5/src/core';
import {
  ButtonView,
  ContextualBalloon,
  clickOutsideHandler
} from 'ckeditor5/src/ui';
import Icon from '../../../icons/bb.svg';
import FormView from './ui/formview';

/**
 * The UI plugin. It introduces the `'bButton'` button and the forms.
 *
 * It uses the
 * {@link module:ui/panel/balloon/contextualballoon~ContextualBalloon contextual balloon plugin}.
 *
 * @extends module:core/plugin~Plugin
 */
export default class BButtonUI extends Plugin {

  /**
   * @inheritDoc
   */
  static get requires() {
    return [ ContextualBalloon ];
  }

  /**
   * @inheritDoc
   */
  init() {
    // Create the balloon.
    this._balloon = this.editor.plugins.get( ContextualBalloon );

    this._addToolbarButton();
    this.formView = this._createFormView();
    this._handleSelection();
  }

  /**
   * Adds the toolbar button.
   *
   * @private
   */
  _addToolbarButton() {
    const editor = this.editor;

    editor.ui.componentFactory.add('bButton', (locale) => {
      const buttonView = new ButtonView(locale);

      // Create the toolbar button.
      buttonView.set({
        label: editor.t('Bootstrap Button'),
        icon: Icon,
        tooltip: true
      });

      // Bind button to the command.
      // The state on the button depends on the command values.
      const command = editor.commands.get('bButton');
      buttonView.bind( 'isEnabled' ).to( command, 'isEnabled' );
      buttonView.bind( 'isOn' ).to( command, 'value', value => !!value );

      // Execute the command when the button is clicked.
      this.listenTo(buttonView, 'execute', () =>
        this._showUI(),
      );

      return buttonView;
    });
  }

  /**
   * Creates the form view.
   *
   * @returns {FormView}
   *   The form view instance.
   *
   * @private
   */
  _createFormView() {
    // The FormView defined in src/ui/formview.js
    const textFormatSettings = this.editor.config.get('c5bb')
    const formView = new FormView(this.editor.locale, textFormatSettings);

    // Form submit handler.
    this.listenTo(formView, 'submit', () => {
      let values = {
        bbLinkText:   formView.textInputView.fieldView.element.value,
        bbLinkUrl:    formView.urlInputView.fieldView.element.value,
        bbLinkClass: 'btn',
        bbTarget:     formView.targetSelectView.getValue(),
        bbGLeft:      formView.glyphLeftInputView.fieldView.element.value,
        bbGRight:     formView.glyphRightInputView.fieldView.element.value,
        bbFALeft:     formView.faLeftInputView.fieldView.element.value,
        bbFARight:    formView.faRightInputView.fieldView.element.value,
      };

      for (let index = 0; index < textFormatSettings.selectors.length; index++) {
        values['bbSelector_' + index] = formView['selectCustom_' + index].getValue();
      }

      this.editor.execute('bButton', values);

      // Hide the form view after submit.
      this._hideUI();
    });

    // Hide the form view after clicking the "Cancel" button.
    this.listenTo( formView, 'cancel', () => {
      this._hideUI();
    } );


    // Hide the form view when clicking outside the balloon.
    clickOutsideHandler( {
      emitter: formView,
      activator: () => this._balloon.visibleView === formView,
      contextElements: [ this._balloon.view.element ],
      callback: () => {
        this._hideUI()
      }
    } );

    return formView;
  }

  /**
   * Adds the {@link #FormView} to the balloon and sets the form values.
   *
   * @private
   */
  _addFormView() {

    this._balloon.add({
      view: this.formView,
      position: this._getBalloonPositionData(),
      balloonClassName: 'c5bb_balloon',
    });

    const command = this.editor.commands.get('bButton');

    const modelToFormFields = {
      bbTarget:     'targetSelectView',
      bbGLeft:      'glyphLeftInputView',
      bbGRight:     'glyphRightInputView',
      bbFALeft:     'faLeftInputView',
      bbFARight:    'faRightInputView',
      bbLinkUrl:    'urlInputView',
      bbLinkText:   'textInputView',
    };

    const textFormatSettings = this.editor.config.get('c5bb')
    for (let index = 0; index < textFormatSettings.selectors.length; index++) {
      modelToFormFields['bbSelector_' + index] = 'selectCustom_' + index;
    }

    // Handle text input fields.
    Object.entries(modelToFormFields).forEach(([modelName, formElName]) => {
      const formEl = this.formView[formElName];
      // Needed to display a placeholder of the elements being focused before.
      formEl.focus();

      const isEmpty = !command.value || !command.value[modelName] || command.value[modelName] === '';

      // Set URL default value.
      if (modelName === 'bbLinkUrl' && isEmpty) {
        formEl.fieldView.element.value = '#';
        formEl.set('isEmpty', false);
        return;
      }

      // Set Text default value.
      if (modelName === 'bbLinkText' && isEmpty) {
        const selection = this.editor.model.document.selection;
        const range = selection.getFirstRange();
        let selectedText = '';
        for (const item of range.getItems()) {
          if (item.data) {
            selectedText += item.data;
          }
        }

        if (selectedText.length) {
          formEl.fieldView.element.value = selectedText;
          formEl.set('isEmpty', false);
          return;
        }
      }

      let found = false
      if (isEmpty) {
        for (let index = 0; index < textFormatSettings.selectors.length; index++) {
          if (modelName == 'bbSelector_' + index) {
            formEl.setValue('');
            found = true;
            break;
          }
        }

        if (!found) {
          switch (modelName) {
            case 'bbTarget':
              formEl.setValue('');
              break;
            default:
              formEl.fieldView.element.value = '';
          }
        }
      } else {
        for (let index = 0; index < textFormatSettings.selectors.length; index++) {
          if (modelName == 'bbSelector_' + index) {
            formEl.setValue(command.value[modelName]);
            found = true;
            break;
          }
        }

        if (!found) {
          switch (modelName) {
            case 'bbTarget':
              formEl.setValue(command.value[modelName]);
              break;
            default:
              formEl.fieldView.element.value = command.value[modelName];
          }
        }
      }
      formEl.set('isEmpty', isEmpty);

    });

    // Reset the focus to the first form element.
    this.formView.focus();
  }

  /**
   * Handles the selection specific cases (right before or after the element).
   *
   * @private
   */
  _handleSelection() {
    const editor = this.editor;

    this.listenTo(editor.editing.view.document, 'selectionChange', (eventInfo, eventData) => {
      const selection = editor.model.document.selection;

      let el = selection.getSelectedElement() ?? selection.getFirstRange().getCommonAncestor();

      if (!el.findAncestor('bButton')) {
        this._hideUI();
        return;
      }

      const positionBefore = editor.model.createPositionBefore(el);
      const positionAfter = editor.model.createPositionAfter(el);

      const position = selection.getFirstPosition();

      const beforeTouch = position.isTouching( positionBefore );
      const afterTouch = position.isTouching( positionAfter );

      // Handle the "border" selection.
      if ((afterTouch && el.name == 'bbLinkText')
        || ((afterTouch || beforeTouch) && ['bbFARight', 'bbGRight'].includes(el.name)) ) {
        editor.model.change(writer => {
          writer.setSelection(editor.model.createPositionAfter(el.findAncestor('bButton')), 'on');
        });
      } else if ((beforeTouch && el.name == 'bbLinkText')
        || ((afterTouch || beforeTouch) && ['bbFALeft', 'bbGLeft'].includes(el.name)) ) {
        editor.model.change(writer => {
          writer.setSelection(editor.model.createPositionBefore(el.findAncestor('bButton')), 'on');
        });
      } else {
        this._showUI();
      }

    });
  }

  /**
   * Shows the UI.
   *
   * @private
   */
  _showUI() {
    this._addFormView();
  }

  /**
   * Hide the UI.
   *
   * @private
   */
  _hideUI() {
    const formView = this.formView;

    // Without this a new form contains the old values.
    if (formView.element) {
      formView.element.reset();
    }

    if (this._balloon.hasView(formView)) {
      this._balloon.remove(formView);
    }

    // Focus the editing view after closing the form view.
    this.editor.editing.view.focus();
  }

  /**
   * Gets balloon position.
   *
   * @returns {{target: (function(): *)}}
   *
   * @private
   */
  _getBalloonPositionData() {
    const view = this.editor.editing.view;
    const viewDocument = view.document;
    let target = null;

    // Set a target position by converting view selection range to DOM.
    target = () => view.domConverter.viewRangeToDom(
      viewDocument.selection.getFirstRange()
    );

    return {
      target
    };
  }

}
