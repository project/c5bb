/**
 * @file
 * Defines the SelectView View class.
 */

/**
 * @module bButton/ui/SelectView
 */

import { View } from "ckeditor5/src/ui";
import { default as OptionView } from './option.js';
import TagView from './tag.js';

/**
 * The SelectView class.
 *
 * @extends module:ui/view~View
 */
export default class SelectView extends View {

  /**
   * @inheritDoc
   */
  constructor( locale, options ) {
    super( locale );

    this.childViewsCollection = this.createCollection();

    this.options = options;
    for (let elm of options.options) {
      let option = new OptionView(locale, { label: elm.label, value: elm.value });
      this.childViewsCollection.add(option);
    }

    this.setTemplate({
      tag: 'div',
      attributes: {
        class: [ 'form-item', 'form-item-select-wrapper' ],
      },
      children: [
        new TagView(this.locale, 'label', {
          text: options.label,
          attributes: {
            class: ['form-item__label'],
          }
        }),
        new TagView(this.locale, 'select', {
          attributes: {
            class: [ 'form-select', 'form-element', 'form-element--type-select', 'ck', 'ck-input' ],
          },
          children: this.childViewsCollection
        })
      ]
    } );
  }

  /**
   * Return array valid values
   */
  getValidValues() {
    const validValues = []
    for (let elm of this.options.options) {
      validValues.add(elm.value)
    }
    return validValues
  }

  setValue(value) {
    let elm = this.element.querySelector('select');
    if (elm) {
      for (let i = 0; i < elm.options.length; i++) {
        let optionValue = '';
        if (elm.options[i].hasAttribute('value')) {
          optionValue = elm.options[i].value
        }
        elm.options[i].selected = optionValue == value;
      }
    }
  }

  getValue() {
    let elm = this.element.querySelector('select');

    if (elm) {
      let optionValue = '';

      for (let i = 0; i < elm.options.length; i++) {
        if (elm.options[i].selected) {
          if (elm.options[i].hasAttribute('value')) {
            optionValue = elm.options[i].value
          }
          break;
        }
      }
      return optionValue;
    }

    return null;
  }

  /**
   * @inheritDoc
   */
  render() {
    super.render();
  }

  /**
   * Focus on the first form element.
   */
  focus() {
    ;
  }

}
