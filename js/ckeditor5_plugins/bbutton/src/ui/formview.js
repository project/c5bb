/**
 * @file
 * Defines the FormView View class.
 */

/**
 * @module bButton/ui/FormView
 */

import {
  ButtonView,
  LabeledFieldView,
  View,
  createLabeledInputText,
  submitHandler,
  FormHeaderView,
  CollapsibleView,
} from "ckeditor5/src/ui";
import { icons } from "ckeditor5/src/core";
import { default as SelectView } from './select.js';
import { default as TagView } from './tag.js';

/**
 * The bButton FormView class.
 *
 * This view displays an editing form.
 *
 * @extends module:ui/view~View
 */
export default class FormView extends View {

  /**
   * @inheritDoc
   */
  constructor( locale, customSelectors = [] ) {
    super( locale );

    // Inputs.
    this.textInputView = this._createInput('Text', { required: true });
    this.urlInputView = this._createInput('URL', {required: true});

    // Selectors
    const selectorsList = [];
    for (let index = 0; index < customSelectors.selectors.length; index++) {
      this['selectCustom_' + index] = this._createSelect(
        customSelectors.selectors[index].label,
        { options: customSelectors.selectors[index].options });
      selectorsList.push(this['selectCustom_' + index]);
    }

    this.targetSelectView = this._createSelect('Target', {options: [
      {label: 'Not Set', value: ''},
      {label: 'Frame', value: 'frame'},
      {label: 'Popup', value: 'popup'},
      {label: 'New Window (_blank)', value: '_blank'},
      {label: 'Topmost Window (_top)', value: '_top'},
      {label: 'Same Window (_self)', value: '_self'},
      {label: 'Parent Window (_parent)', value: '_parent'},
    ]});

    // Icon classes
    this.glyphLeftInputView = this._createInput('Left Icon', { required: false });
    this.glyphRightInputView = this._createInput('Right Icon', { required: false });
    this.faLeftInputView = this._createInput('Left Icon', { required: false });
    this.faRightInputView = this._createInput('Right Icon', { required: false });

    // Create the save button.
    this.saveButtonView = this._createButton(
      'Save', icons.check, ['button', 'form-submit', 'ck-button-save']
    );

    // Triggers the submit event on entire form when clicked.
    this.saveButtonView.type = 'submit';

    // Create the cancel button.
    this.cancelButtonView = this._createButton(
      'Cancel', icons.cancel, ['button', 'form-submit', 'ck-button-cancel']
    );

    // Delegate ButtonView#execute to FormView#cancel.
    this.cancelButtonView.delegate( 'execute' ).to( this, 'cancel' );

    let iconsGroup = new CollapsibleView(this.locale, [
      new TagView(this.locale, 'div', {
        children: [
          new TagView(this.locale, 'b', { text: 'Bootstrap Glyphicon'}),
          new TagView(this.locale, 'br'),
          'List of Icons,. e.g. glyphicon-pencil'
        ]
      }),
      this.glyphLeftInputView,
      this.glyphRightInputView,
      new TagView(this.locale, 'div', {
        children: [
          new TagView(this.locale, 'b', { text: 'Font Awesome'}),
          new TagView(this.locale, 'br'),
          'List of Icons, e.g. fa-arrow-right, fa-volume-up, fa-play'
        ]
      }),
      this.faLeftInputView,
      this.faRightInputView,
    ]);
    iconsGroup.set('label', 'Icons (Glyph and FA)');

    this.childViewsCollection = this.createCollection([
      new FormHeaderView(this.locale, { label: 'Bootstrap Buttons'}),
      new TagView(this.locale, 'div', {
        attributes: {
          class: [],
          tabindex: '-1'
        },
        children: [
          new TagView(this.locale, 'table', {
            attributes: {
              width: '100%',
              cellpadding: '0',
              cellspacing: '15',
              style: 'margin: 0'
            },
            children: [
              new TagView(this.locale, 'tr', {
                children: [
                  new TagView(this.locale, 'td', {
                    attributes: {
                      style: 'vertical-align: top',
                    },
                    children: [
                      this.textInputView,
                      this.urlInputView,
                      ...selectorsList,
                      this.targetSelectView,
                    ]
                  }),
                  new TagView(this.locale, 'td', {
                    attributes: {
                      style: 'vertical-align: top',
                    },
                    children: customSelectors.showIconSettings == 1
                      ? [iconsGroup]
                      : []
                  }),
                ]
              }),
              new TagView(this.locale, 'tr', {
                children: [
                  new TagView(this.locale, 'td', {
                    attributes: {
                      style: 'vertical-align: top',
                      colspan: '2'
                    },
                    children: [
                      this.saveButtonView,
                      this.cancelButtonView
                    ]
                  }),
                ]
              })
            ]
          }),

        ]
      })
    ]);

    this.setTemplate( {
      tag: 'form',
      attributes: {
        class: [ 'ck', 'ck-bbutton-link-form', 'ck-reset_all-excluded' ],
        // https://github.com/ckeditor/ckeditor5-image/issues/40
        tabindex: '-1'
      },
      children: this.childViewsCollection
    } );

  }

  /**
   * @inheritDoc
   */
  render() {
    super.render();

    // Submit the form when the user clicked the save button
    // or pressed enter in the input.
    submitHandler( {
      view: this
    } );
  }

  /**
   * Focus on the first form element.
   */
  focus() {
    // this.childViewsCollection.get(1).children.get(0).focus();
  }

  /**
   * Creates an input field.
   *
   * @param {string} label
   *   Input field label.
   * @param {object} options
   *   Options.
   *
   * @returns {module:ui/labeledfield/labeledfieldview~LabeledFieldView}
   *   The labeled field view class instance.
   *
   * @private
   */
  _createInput(label, options = {}) {
    const labeledFieldView = new LabeledFieldView(this.locale, createLabeledInputText);
    labeledFieldView.label = label;

    // Sets the required attribute when needed.
    if (options.required && options.required === true) {
      labeledFieldView.fieldView.extendTemplate({
        attributes: {
          required: true,
          class: ['form-text', 'form-element']
        }
      });
    }

    labeledFieldView.setTemplate({
      tag: 'div',
      attributes: {
        class: [ 'form-item' ],
      },
      children: [labeledFieldView.template]
    });

    return labeledFieldView;
  }

  /**
   * Creates a select field.
   *
   * @param {string} label
   *   Input field label.
   * @param {object} options
   *   Options.
   *
   * @returns {module:ui/dropdown/DropdownView}
   *   The labeled field view class instance.
   *
   * @private
   */
  _createSelect(label, options = {}) {
    options.label = label
    return new SelectView(this.locale, options);
  }

  /**
   * Creates button.
   *
   * @param {string} label
   *   Button label.
   * @param {module:ui/icon/iconview~IconView} icon
   *   Button icon.
   * @param {string} className
   *   HTML class.
   *
   * @returns {module:ui/button/buttonview~ButtonView}
   *   The button view class instance.
   *
   * @private
   */
  _createButton( label, icon, className ) {
    const button = new ButtonView();

    button.set({
      label,
      icon,
      class: className,
      tooltip: true,
      withText: true
    });

    return button;
  }

}
