/**
 * @file
 * Defines the OptionView View class.
 */

/**
 * @module bButton/ui/OptionView
 */

import {
  View,
} from "ckeditor5/src/ui";

/**
 * The OptionView class.
 *
 * @extends module:ui/view~View
 */
export default class OptionView extends View {

  /**
   * @inheritDoc
   */
  constructor( locale, options ) {
    super( locale );

    this.setTemplate( {
      tag: 'option',
      attributes: {
        'value': options.value
      },
      children: [{ text: options.label } ]
    });

  }

  /**
   * @inheritDoc
   */
  render() {
    super.render();
  }

}
